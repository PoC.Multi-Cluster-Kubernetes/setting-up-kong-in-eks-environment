
# WIP
# What it will be done here

    https://blog.mykaarma.dev/highly-available-kong-setup-on-aws-eks-4af21e5fe89f


## Setting Up Kong in EKS Environment

## How Do I Choose? API Gateway vs. Ingress Controller vs. Service Mesh

- [ ] [Article explaning API Gateways - Article nginx] https://www.nginx.com/blog/how-do-i-choose-api-gateway-vs-ingress-controller-vs-service-mesh/

## What Is an API Gateway?

An API gateway routes API requests from a client to the appropriate services. 
But a big misunderstanding about this simple definition is the idea that an API gateway is a unique piece of technology. 
It’s not. Rather, “API gateway” describes a set of use cases that can be implemented via different types of proxies – 
most commonly an ADC or load balancer and reverse proxy, and increasingly an Ingress controller or service mesh.


## Important articles in this domain

- [ ] [Article traefik] https://dev.to/aurelievache/deploying-a-kubernetes-cluster-aws-eks-an-api-gateway-secured-by-mtls-with-terraform-external-dns-traefik-part-1-34nd
- [ ] [Article kong] https://blog.mykaarma.dev/highly-available-kong-setup-on-aws-eks-4af21e5fe89f
- [ ] [Article traefik] https://traefik.io/blog/reverse-proxy-vs-ingress-controller-vs-api-gateway/
- [ ] [Article traefik] https://www.solo.io/topics/kubernetes-api-gateway/#What-is-the-Kubernetes-API-gateway-project